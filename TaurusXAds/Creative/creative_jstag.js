function scriptFallback(scriptContent) {
    return function() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.charset = 'utf-8';
        script.textContent = scriptContent;
        document.head.appendChild(script);
    }
}

var SuccessFallback = scriptFallback('window.location = "https://access.satorisys.ai";CreativeAds.onAdLoaded();');
var FailFallback = scriptFallback('window.location = CreativeAds.onAdFailedToLoad(0, "some error")');

SuccessFallback()